<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'jhe_basic_template',
    'Configuration/TypoScript',
    'Basic Template'
);
