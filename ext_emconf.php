<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Basic Template Extension',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Jan Helke',
    'author_email' => 'typo3@quintanion.net',
    'author_company' => 'quintanion.net',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
);
